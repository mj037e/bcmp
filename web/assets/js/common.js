$(document).ready(function(){
	openButton('#process_notification');
    openButton('#process_request');
});

function openButton(target){
    //버튼을 비활성화 시키려면 버튼에 readonly 를 넣어주세요. 버튼색상이 바뀝니다.
    var html = '<div class="btn-wrap">'; 
    if(target === '#process_notification'){
        html += '<input type="button" value="OZ e-Form">'
        html += '<input type="button" value="완료통보">'
        html += '<input type="button" value="재검통보">'
    }else if(target ==='#process_request'){
        html += '<input type="button" value="OZ e-Form">'
        html += '<input type="button" value="검측요청">'
    }
    html += '</div>'

    var transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend';
 
    $(target + " .btn-wrap").one(transitionEnd, function(){
        console.log( this.id + "애니메이션 완료" );
    });
    
    $(target + " .row").on('click',function(){
        var isSelect = $(this).hasClass('selec');
        if(isSelect){
            $(this).removeClass('selec');
            $(this).find('.btn-wrap').remove();
        }else{
            $(this).siblings('.selec').find('.btn-wrap').remove();
            $(this).siblings('.selec').removeClass('selec')
            $(this).addClass('selec');
            $(this).append(html);
        }
        $(this).on('click','input[type=button]',function(e){
            e.stopPropagation();
        });
    });
}